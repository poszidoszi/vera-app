import React from 'react';

class Login extends React.Component {

  state = { userName: '', password: ''}

  handleSubmit () {}

  render () {
    return (
      <div>
        <h4>Belépés</h4>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <input
              name="name"
              type="text"
              placeholder="Felhasználónév"/>
          </div>

          <div className="form-group">
            <input
              name="password"
              type="text"
              placeholder="Jelszó"/>
          </div>

          <div>
            <input type="submit" value="Belépés" />
          </div>
        </form>
      </div>
    );
  }
}

export default Login